
=======================

This module contains
* The reStructuredText sources of all QUIPs (``quip-*.rst``)
* The scripts to process them (in ``scripts/``)
* A copy of Planet Qt as a sub-module

See the generated HTML for further details, or the preamble for
``html/quip-000.html`` defined in ``scripts/gen-quip-0000.py`` for a
preview of what this is all about.

Generating HTML from QUIPs
--------------------------

If you want the output to include the Planet Qt style,
you'll need this module checked out, along with its sub-module: see
`Planet Qt and the template`_ for how to get Planet Qt checked out and
up to date.
Alternatively, you can set the ``QUIP_TEMPLATE`` make variable
to use another HTML template.

You can generate the HTML using ``make``, provided you have GNU
``make``, ``python3`` and its ``docutils`` package installed.

By default, the ``Makefile`` generates HTML in an ``html/`` peer
directory of this module's checkout.
You can override that on the ``make`` command-line by setting
``HTMLDIR`` to some directory outside this source tree.
The HTML shall then be generated or updated in that directory.
Alongside the HTML, ``make`` will also place symlinks to the
supporting material from Planet Qt, referenced by the generated HTML.
You can then point a web browser at the result and review it.

Checking
~~~~~~~~

It may be constructive to use checklink_ on the resulting HTML.
On Debian, this is available for local use from the package named
``w3c-linkcheker``; other systems probably have something similar.
It no longer supports checking ``file://`` URLs, or local files, so
you'll need to generate the HTML where you (at least) can see it on
the web.
If that's by running a local web-server on a non-public host, you may
need to comment out its ``Allow_private_IPs`` setting in
``/etc/w3c/checklink.conf`` to make the following work.

Then pass the URL for the generated HTML directory to ``checklink``
with command-line options ``-qbr``, for example:

 checklink -qbr http://localhost/~test/quips/

Alternatively, set ``HTMLURL`` to that top-level URL on the
command-line to ``make check`` (which will filter out some unhelpful
warnings from ``perl`` about the ``checklink`` script itself), for
example:

 make HTMLURL=http://localhost/~test/quips/ check

If that finds any broken links, check to see whether they derive from
the template (report to the planetqt maintainers) or from the source
of the QUIP (``quip-*.rst``, report to the relevant QUIP's author).
Some certificate errors for ``account.qt.io`` may arise; they're not a
flaw in the generated HTML for QUIPs, so just manually check you can
visit the URLs it mentions for this.
As long as you can reach them, the problem is just with the script
having a limited implementation of ``https``, most likely.

.. _checklink: http://validator.w3.org/docs/checklink.html

Minimal template
----------------

In ``templates/`` you will find a minimal html to generate the QUIPs
without any styling. This template can be used like:

  make QUIP_TEMPLATE="templates/simple_template.html"

but by default, a template derived from Planet Qt is used.

Planet Qt and the template
--------------------------

When you update this module, you should ``git submodule update
--init`` to keep its ``planetqt/`` submodule in sync.
This shall normally be on a revision for which the scripts are known
to work.

Updating
~~~~~~~~

In the ``planetqt/`` submodule, you can ``git checkout master`` and
``git pull --ff-only`` to get an up-to-date version.
This might work fine with the existing scripts and templates, in which
case it's fine to ``git add planetqt`` in the super-module and commit.

However, if ``planetqt/template.qt`` has changed, it's possible the
scripts shall fail to generate ``template.html`` from it.
The ``Makefile`` first attempts this by applying
``scripts/template.diff`` to produce ``scripts/template.html``.
When it fails, a partially-patched ``scripts/template.html`` should
result in a ``scripts/template.rej`` file, containing the unapplied
hunks from the patch.

You'll need to manually adapt the patch's modifications to produce a
sensible ``template.html`` (see below) based on the new
``template.qt`` from Planet Qt.

Once you have a usable ``template.html``, move it from ``scripts`` to
this top-level directory and run::

 $ make patch

You might then want to move your working ``template.html`` to safety,
while you check that building works; your test build shall, otherwise,
stomp your ``template.html`` in the process of generating the one it
uses.
If it all works nicely, commit your updated ``scripts/template.diff``
along with the updated ``planetqt/``.

You can also use this process, manually updating ``template.html`` and
updating the patch, to work round problems with the parts of Planet Qt
we use.
For example, problems in the style-sheets can be fixed by patching a
style element into the template, after the style-sheets.

How the template works
~~~~~~~~~~~~~~~~~~~~~~

The template file, ``template.html``, is mostly used verbatim in the
generated web page for each QUIP, with a few tokens replaced, each of
which starts with a % character.
In particular, this means that any URLs in ``planetqt/template.qt``
that contain %-encoded tokens (as part of the usual URL-encoding) need
the % in each such token doubled up, to a %% that the reStructuredText
engine shall convert back to a single % in the generated web page.

The other tokens replaced are ``%(name)`` for various choices of
``name``; most of these are set up in ``scripts/quip2html.py`` by
``QuipWriter::interpolation_dict()`` adding entries to its ``subs``
mapping.
An entry for ``%(stylesheet)`` is generated from
``QuipWriter::settings_default_overrides['stylesheet_path']`` and
there may be more entries automagically added by the ``docutils``
infrastructure, 'though we don't presently exercise these.

Complications
~~~~~~~~~~~~~

When updating ``template.html`` from a new ``template.qt``, it is best
to change as little as possible: keep the ``diff`` small so that
``patch`` has a better chance of applying it.
Generally, that means only changing what you must, nothing more.
If you think the web-design of planetqt can be improved on, submit
changes to it, don't complicate your differences from it with changes
the next person updating this directory might not understand.

Under ``planetqt/website`` there are assorted files that shall be
referenced from your ``template.html``, expecting them to be in the
same directory.
There's also a sub-directory ``images/``.
All of these need to be visible in the directory containing the
generated HTML; the ``Makefile`` rules for its default target
(``html``) should take care of this.

You'll need to keep the ``WEBLINK`` list in the ``Makefile`` up to
date; it lists the names of files and directories, within (and
relative to) ``planetqt/website``, that are needed by the template.

If the new planetqt template contains a copyright notice, consult with
planetqt's owners about whether they're happy to have the adapted
template, without that notice, and accompanying linked content,
released under this module's CC0 license.
If they are, remove their license as part of adapting the template;
otherwise, work out some sensible way to satisfy their needs within
the framework of this module; you may need to find some other source
than planetqt for templates and styling.

Resist the temptation to replace the contents of planetqt/ with links
to files on planet.qt.io: doing so would make it necessary to keep
permanently in sync, instead of only updating when convenient.
