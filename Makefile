# Maintenance of the QUIP module and its generated HTML docs.
# Expects GNU make, run on Unix (so #!/usr/bin/env ... scripts work)
# See ReadMe.rst for further details.
default: html
.PHONY: default html

HTMLDIR ?= ../html
# Don't print setup commands as they are run, unless VERBOSE is set:
ifeq ($(VERBOSE),)
Q ?= @
endif

RST := $(wildcard quip-[0-9][0-9][0-9][0-9]*.rst)
RSTHTML := $(RST:%.rst=$(HTMLDIR)/%.html)
HTML := $(HTMLDIR)/quip-0000.html $(RSTHTML)
ifeq ($(QUIP_TEMPLATE),)
WEBLINK := favicon.ico \
	cookiebar-x.png cookie_small.png list_arrow.png theqtcompany.png \
	extras.js main.js modernizr.custom.84855.js \
	online.css style.css \
	images
LINK := $(HTMLDIR)/.web $(HTMLDIR)/index.html $(WEBLINK:%=$(HTMLDIR)/%)
endif

html: $(HTML) $(LINK)
$(HTML) $(LINK): $(HTMLDIR)/.ready

%/.ready:
	$Q mkdir -p $(@D) && touch $@

$(HTMLDIR)/index.html: $(HTMLDIR)/quip-0000.html
	$Q [ -h $@ ] || (cd $(@D) && ln -s $(<F) $(@F))
ifeq ($(QUIP_TEMPLATE),)
$(WEBLINK:%=$(HTMLDIR)/%) $(WEBLINK:%=$(HTMLDIR)/.web/%): $(HTMLDIR)/.web
$(WEBLINK:%=$(HTMLDIR)/%): $(HTMLDIR)/%: $(HTMLDIR)/.web/%
	$Q [ -h $@ ] || (cd $(@D) && ln -s .web/$(@F) $(@F))
$(HTMLDIR)/.web: planetqt/website
	$Q [ -h $@ ] || (cd $(@D) && ln -s $(abspath $<) $(@F))
endif

$(HTMLDIR)/quip-0000.html: scripts/gen-quip-0000.py scripts/quip2html.py $(RST) template.html
	scripts/gen-quip-0000.py $(RST) | scripts/quip2html.py - >$@
$(RSTHTML): $(HTMLDIR)/%.html: %.rst scripts/quip2html.py template.html
	scripts/quip2html.py $< >$@

ifeq ($(QUIP_TEMPLATE),)
.INTERMEDIATE: scripts/template.html
template.html: scripts/template.diff planetqt/template.qt
	$Q patch -l -p0 -i $< -o scripts/$@ && mv scripts/$@ $@
# We only stomp template.html if patch succeeds.
else
template.html: $(QUIP_TEMPLATE)
	$Q rm -f $@; ln -s $< $@
endif

.PHONY: clean
clean:
	$Q rm -f scripts/template.html template.html
	$Q echo "Remove the generated HTML (perhaps in $(HTMLDIR)) yourself"

# For use during an update:
.PHONY: patch
patch: template.html planetqt/template.qt
	$Q if cp $< scripts/$< && diff -bu planetqt/template.qt scripts/$< >scripts/template.diff; \
	   then echo Empty patch; else [ $$? -eq 1 ]; fi
# Does not declare dependency of template.diff on prerequisites, to avoid circular dependency.

planetqt/template.qt planetqt/website:
	$Q echo "Please see ReadMe.rst for submodule checkout instructions"; false
# The false is to stop the build dead if planetqt/ is missing.

# TODO: we need a non-heroku site (QTWEBSITE-978)
HTMLURL ?= http://quips-qt-io.herokuapp.com/
.PHONY: check
check: html
	checklink -qbr $(HTMLURL) 2>&1 | grep -wv 'Use of uninitialized value .*/checklink line'
