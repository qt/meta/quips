QUIP: 23
Title: Qt-Security header in source code files
Author: Dimitrios Apostolou
Status: Active
Type: Process
Content-Type: text/x-rst
Created: 2024-07-03
Post-History: https://lists.qt-project.org/pipermail/development/2024-July/045465.html


Qt-Security header in source code files
=======================================

Some files in Qt may contain, near the top, one single-line comment starting
with the string "Qt-Security". The full syntax is described later in this
document.  Here is an example of what such a header may look like in
practice::

  // Qt-Security score:critical reason:data-parser

Purpose
-------

The idea is to identify the files containing code where bugs are more likely
to cause security issues.  For example, code that is parsing input
from untrusted sources, or a protocol implementation.

This information may then be used by the code-review and CI system to enforce
special handling for such files, for example extra scrutiny in the review
process, stricter test requirements, enabling sanitizers and doing fuzz
testing in the CI process etc. Opposite special handling might be enforced
for files marked as security-insignificant, for example skipping static
analysis.

Criteria for choosing the right ``score``
-----------------------------------------

By default all code is presumed to be significant to security,
described by ``score:significant``. The header can be omitted from files
to which this applies.

Security-critical files should be marked with ``score:critical``. This applies to
files which contain code that:

* parses external data or handles input from untrusted sources directly
* implements network protocols (this is actually a subcatecory of the above)
* implements cryptography or handles relevant libraries.

Usually these are ``.cpp`` files, but if other files contain such code, they
should be marked too. For example, C++ header files that contain inline
functions or macros that fulfill the above criteria.

A file with insignificant impact on security may be marked with ``score:insignificant``.
This is mostly intended for certain tools and tests, where a crash is very
unlikely to cause a security vulnerability in Qt.

Syntax
------

The syntax of the header is intended to be both readable by humans, as well as
easily parseable by a tool.  As mentioned earlier, the syntax is::

  // Qt-Security score:LEVEL reason:some-reason

The Qt-Security comment line must be located towards the beginning of the
file, before any source code. By preference it's usually placed right after
the SPDX license header.  The content of the header must all appear in one
line, without any line breaks in between.

* ``score`` gives a score.  For now the only acceptable values are:

  insignificant
    Code where bugs are not expected to directly cause vulnerabilities.

  significant
    Security significant code.
    **This is the default level**, implied for files lacking the
    Qt-Security header.

  critical
    Security critical code, where bugs are more likely to cause
    vulnerabilities and security issues in general.

* ``reason`` contains a keyword explaining the main reason for the score.
  Valid values are strings of the form ``[-a-z0-9]+`` for example:

  - data-parser
  - network-protocol
  - cryptography
